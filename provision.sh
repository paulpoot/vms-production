#!/bin/bash

echo "Adding Docker Repository..."
# Add new gpg key
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

# Entry for Ubuntu 14.04 LTS
sudo echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" >> /etc/apt/sources.list.d/docker.list

# Update package index
sudo apt-get update

# Purge old repo
sudo apt-get purge lxc-docker

# Verify pulling from right repository
sudo apt-cache policy docker-engine

# Update package manager
sudo apt-get update

echo "Installing packages..."
# Install recommended package
sudo apt-get install linux-image-extra-$(uname -r)

# Install Docker
sudo apt-get install docker-engine

# Start Docker daemon
sudo service docker start

# Verify correct installation
sudo docker run hello-world

# Get current user
currentuser=$(whoami)

# Add current user to docker group
sudo usermod -aG docker $currentuser

sudo apt-get install python-pip

sudo pip install docker-compose

echo "Finished Provision Script for user $currentuser!"
