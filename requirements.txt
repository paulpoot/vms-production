# Install Python modules

# Newest Django
django==1.9.1

# Postgres Connection
psycopg2==2.5.4

# Serving Static files
dj-static==0.0.6

python-memcached==1.53

gunicorn==19.1.1

################
# VMS Specific #
################

# API Connection
requests==2.9.1

# PDF Generating
reportlab==3.2.0
