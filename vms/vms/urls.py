################################################################################
###
###         urls.py
###         Last updated: 21/01/2016
###
###         Handles URLS.
###
################################################################################
from django.conf.urls import include, url
from django.contrib import admin
from website import views

urlpatterns = [
    # Common Pages
    url(r'^$', views.index),
    url(r'^metadata/$', views.metadata, name='metadata'),
    url(r'^downloadmetadata/$', views.get_metadata, name='download_metadata'),
    # Reports
    url(r'^reports/(?P<vehicleid>[0-9]+)/$', views.generate_reports, name='generate_reports'),
    # Vehicles
    url(r'^vehicles/(?P<vehicleid>[0-9]+)/$', views.vehicle, name='vehicles'),
    url(r'^vehicles/search/', views.search_vehicles),
    url(r'^vehicles/refresh/', views.populate_vehicles),
    url(r'^vehicles/favourite/(?P<vehicleid>[0-9]+)/(?P<update>[A-Z][a-z]+)/$', views.update_favourite),
    url(r'^vehicles/comment/(?P<vehicleid>[0-9]+)/$', views.update_comment),
    url(r'^vehicles/$', views.vehicle_overview, name='vehicle_overview'),
    # User Administration
    url(r'^login/', views.user_login, name='login'),
    url(r'^accounts/login/', views.user_login),
    url(r'^logout/', views.user_logout),
    url(r'^admin/', admin.site.urls),
]
