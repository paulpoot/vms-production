################################################################################
###
###         wsgi.py
###         Last updated: 21/01/2016
###
###         Handles WSGI config and sends requests through dj_static
###
################################################################################
import os
from django.core.wsgi import get_wsgi_application
from dj_static import Cling
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vms.settings")

application = Cling(get_wsgi_application())
