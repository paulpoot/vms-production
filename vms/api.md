## Processor Usage
https://vms-hro.janhagenaar.eu/api/Monitoring/GetProcessorUsageInfo?unitId=14100064

## Processor Temperature
https://vms-hro.janhagenaar.eu/api/Monitoring/GetProcessorTemperature?unitId=999

## GPS Temperature
https://vms-hro.janhagenaar.eu/api/Monitoring/GetGpsInfo?unitId=14100064

## Memory Usage
https://vms-hro.janhagenaar.eu/api/Monitoring/GetMemoryUsageInfo?unitId=14100064

## Unit IDs
https://vms-hro.janhagenaar.eu/api/Position/GetUniqueUnitIds

## Speed Time info
https://vms-hro.janhagenaar.eu/api/Position/GetSpeedTimeInfo?unitId=14100064

## Operation Time
https://vms-hro.janhagenaar.eu/api/Event/GetOperationTime?unitId=14100064

## Driven Distance
https://vms-hro.janhagenaar.eu/api/Position/GetDrivenDistance?unitId=14100064
