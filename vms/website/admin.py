################################################################################
###
###         admin.py
###         Last updated: 21/01/2016
###
###         Handles Admin interface.
###
################################################################################
from django.contrib import admin
from website.models import Vehicle

################################################################################
###
###         VehicleAdmin
###
###         Determines how Vehicle class is represented in UI.
###
################################################################################
class VehicleAdmin(admin.ModelAdmin):
    list_display = ('unitid', 'favourite')

################################################################################
###
###         register
###
###         Registers classes in Admin interface.
###
################################################################################
admin.site.register(Vehicle, VehicleAdmin)
