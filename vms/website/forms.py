################################################################################
###
###         forms.py
###         Last updated: 21/01/2016
###
###         Handles webforms.
###
################################################################################
from django import forms

class unitidForm(forms.Form):
    unitID = forms.CharField(label='unitID ', max_length=100)
