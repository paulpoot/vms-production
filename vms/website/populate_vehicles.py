################################################################################
###
###         populate_vehicles.py
###         Last updated: 21/01/2016
###
###         Handles population of database.
###
################################################################################
import json
import requests
import os
import django
from website.models import Vehicle
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vms.settings')
django.setup()

################################################################################
###
###         populate()
###
###         Requests unitids and adds them to database if they don't exist
###         already.
###
################################################################################
def populate():

    # URL to connect to
    url = "https://vms-hro.janhagenaar.eu/api/Position/GetUniqueUnitIds"

    try:
        # Connect
        response = requests.get(url)

        # Convert response to dictionary object
        json_response = response.json()

        # Loop through results
        for result in json_response:

            # Check if vehicle is already in database to prevent favourite and comments attributes from being reset
            if not Vehicle.objects.filter(unitid=result).exists():
                add_vehicle(
                  unitid = result,
                  favourite = 0,
                  comments = "There are no comments about this vehicle yet.",
                )

    # Catch URLError exception
    except requests.exceptions.RequestException, e:
        print "Error querying API: ", e

################################################################################
###
###         add_vehicle(unitid, favourite, comments)
###
###         Adds vehicle to database.
###
################################################################################
def add_vehicle(unitid, favourite, comments):
    v = Vehicle.objects.get_or_create(unitid=unitid)[0]
    v.favourite=favourite
    v.comments=comments
    v.save()
    return v
