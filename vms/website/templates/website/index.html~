<!doctype html>
<html lang=en>

<head>

  {% load staticfiles %}

  <meta charset=utf-8>
  <title>VMS - Vehicle Management System</title>
  <!-- Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700|Hind' rel='stylesheet' type='text/css'>
  <!-- Stylesheets -->
  <link rel="stylesheet" type="text/css" href="{% static 'website/css/bootstrap.css' %}">
  <link rel="stylesheet" type="text/css" href="{% static 'website/css/animate.css' %}">
  <link rel="stylesheet" type="text/css" href="{% static 'website/css/vms.css' %}">

</head>

<body>


  <header>
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">

        <!-- Logo and toggle -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="index">VMS</a>
        </div>

        <!-- Navigation links -->
        <div class="collapse navbar-collapse" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="login" class="btn btn-secondary">Sign in</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->

      </div>
    </nav>
  </header>

  <div class="jumbotron big">
    <div class="container animated slideInLeft">
      <h1>Vehicle Management System</h1>
      <p>VMS provides a great featureset for keeping track of vehicles. We process a lot of data and can provide useful metadata.</p>
      <p><a class="btn btn-secondary btn-lg" href="#about" role="button">Learn more</a></p>
    </div>
  </div>

  <div class="container-fluid primarydark-bg link-bar">
    <ul>
      <li><a href="#about">About VMS</a></li>
      <li><a href="#metadata">Metadata</a></li>
      <li><a href="#signup">Sign up</a></li>
    </ul>
  </div>

  <section class="about">
    <a id="about"></a>
    <div class="container">
      <h1 class="animated slideInLeft">Big Data. <span class="headbg primary-bg">Big Results.</span></h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam iaculis ullamcorper purus, nec sodales dolor dignissim vel. Vestibulum vel tortor vitae nisi sodales elementum at et magna. Donec hendrerit quam quis ipsum ullamcorper, sed aliquam nunc dignissim. Suspendisse eget urna in tellus tincidunt volutpat. Praesent congue vestibulum ex, quis gravida nunc mattis et. Aliquam placerat at dui sed suscipit. Aenean luctus ante nec urna pellentesque laoreet. Mauris at odio eu odio rutrum sollicitudin. Donec lacus tortor, ultrices a condimentum eget, ultricies eget tellus. Suspendisse a tempus felis. In tempus, urna eu sollicitudin varius, mi nisi bibendum sapien, imperdiet consectetur leo lectus et metus. Vestibulum pellentesque at ex sit amet facilisis. Aliquam posuere auctor massa eu ullamcorper. Vivamus eget lacinia ipsum. Fusce venenatis lacus nec neque tincidunt, ut ornare arcu interdum. Suspendisse mollis euismod euismod. </p>
    </div>
  </section>

  <section class="metadata primary-bg">
    <a id="metadata"></a>
    <div class="container">
      <h1 class="animated slideInRight">Data you can <span class="headbg secondary-bg">build on.</span></h1>
      <div class="row">
        <div class="col-xs-12 col-md-4 graph">
          <canvas id="ignitions" max-width="100%"></canvas>
          <h2>Ignition</h2>
        </div>
        <div class="col-xs-12 col-md-4 graph">
          <canvas id="speed" max-width="100%"></canvas>
          <h2>Speed</h2>
        </div>
        <div class="col-xs-12 col-md-4 graph">
          <canvas id="reception" max-width="100%"></canvas>
          <h2>Reception</h2>
        </div>
    </div>
  </section>

  <section class="signup">
    <a id="signup"></a>
    <div class="container">
      <h1 class="animated slideInRight">Data <span class="headbg primary-bg">for you.</span></h1>
      <p>You know what our data can do. Are you ready to develop amazing new ideas based on this data? Sign up and tell us about your plans.</p>
      <a class="btn btn-primary btn-lg" href="login.html" role="button">Sign up</a>
  </section>

  <footer class="primary-bg">
    <div class="container">
      <a class="brand" href="index.html">VMS</a>
    </div>
  </footer>

  <!-- Scripts -->
  <script src="{% static 'website/js/jquery-2.1.4.min.js' %}"></script>
  <script src="{% static 'website/js/bootstrap.js' %}"></script>
  <script src="{% static 'website/js/moment.js' %}"></script>
  <script src="{% static 'website/js/chart.min.js' %}"></script>

  <!-- Chart Setup -->
  <script>
  Chart.defaults.global.showScale = false;
  Chart.defaults.global.showTooltips = false;
  Chart.defaults.global.responsive = false;
  Chart.defaults.Doughnut.segmentShowStroke = false;

  var ignitionData = {
      labels: ['Vehicle A', 'Vehicle B', 'Vehicle C', 'Vehicle D', 'Vehicle E'],
      datasets: [
          {
              label: '',
              fillColor: '#fefefe',
              strokeColor: "rgba(255,255,255,1)",
              data: [2500, 1902, 1041, 1504, 800]
          }
      ]
  };

  var speedData = {
    labels: ["", "", "", "", "", "", ""],
    datasets: [
        {
            label: "",
            fillColor: "rgba(255,255,255,0.2)",
            strokeColor: "rgba(255,255,255,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [57, 88, 112, 90, 105, 60, 30]
        }
    ]
  };

  var receptionData = [
      {
          value: 43,
          color:"#fefefe",
          highlight: "#fefefe",
          label: "Great"
      },
      {
          value: 51,
          color:"#3769b0",
          highlight: "#3769b0",
          label: "Good"
      },
      {
          value: 8,
          color: "#053882",
          highlight: "#053882",
          label: "Bad"
      }
  ]


  var context = document.getElementById('ignitions').getContext('2d');
  var ignitionChart = new Chart(context).Bar(ignitionData);

  var context = document.getElementById('speed').getContext('2d');
  var speedChart = new Chart(context).Line(speedData);

  var context = document.getElementById('reception').getContext('2d');
  var receptionChart = new Chart(context).Doughnut(receptionData);
  </script>
</body>

</html>
