################################################################################
###
###         tests.py
###         Last updated: 21/01/2016
###
###         Python module for testing purposes. Run tests by calling manage.py
###			test. Testcases are automatically gathered and executed.
###
################################################################################
from django.test import TestCase
from django.test import Client
from django.core import exceptions
from django.core.urlresolvers import reverse
from website.models import Vehicle
from website.populate_vehicles import populate

# Testclass for the Views. Checks if status codes are correct.
class TestViews(TestCase):

	def test_metadata(self):
		response = self.client.get(reverse('metadata'))
		self.assertEqual(response.status_code, 200)

	def test_vehicle_overview(self):
		response = self.client.get(reverse('vehicle_overview'))
		self.assertEqual(response.status_code, 302)

	def test_login(self):
		response = self.client.get(reverse('login'))
		self.assertEqual(response.status_code, 200)

# Tesclass for the population feature. Checks if existing objects are not overwritten when populating.
class TestPopulation(TestCase):

	def test_persistence(self):
		original_comments = "This is a very original comment."
		Vehicle.objects.create(unitid=999, favourite=1, comments=original_comments)

		populate()

		self.assertEqual(Vehicle.objects.get(unitid=999).comments, original_comments, "\nThe original comment is lost.")
