################################################################################
###
###         api.py
###         Last updated: 21/01/2016
###
###         Python module for API connection. Builds URL and creates objects
###         for the webpages.
###
################################################################################
import json
import requests

################################################################################
###
###         run_query(type, unitid)
###
###         Builds URL for API connection and returns parsed data.
###
################################################################################

def run_query(type, unitid):

    # Base URL
    root_url = "https://vms-hro.janhagenaar.eu/api/"

    # Determines correct url depending on type
    if type == "ProcessorUsage":
        data_type = "Monitoring/GetProcessorUsageInfo"
    elif type == "ProcessorTemperature":
        data_type = "Monitoring/GetProcessorTemperature"
    elif type == "MemoryUsage":
        data_type = "Monitoring/GetMemoryUsageInfo"
    elif type == "GPSTemperature":
        data_type = "Monitoring/GetGpsInfo"
    elif type == "DrivenDistance":
        data_type = "Position/GetDrivenDistance"
    elif type == "OperationTime":
        data_type = "Event/GetOperationTime"
    else:
        print("Error: Invalid type input.")

    # Builds url
    request_url = root_url + data_type + "?unitId=" + str(unitid)

    results = []

    try:
        # Connect
        response = requests.get(request_url)

        # Convert response to dictionary object
        json_response = response.json()

        # Loop through results for every data type
        if type == "ProcessorUsage":
            for result in json_response['ProcessorUsage']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Usage': result['Usage']
                })
            results.append({
                'Usage': json_response['AverageUsage']
            })

        if type == "ProcessorTemperature":
            for result in json_response['ProcessorTemperature']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Temperature': result['Temperature']
                })
            results.append({
                'Temperature': json_response['AverageTemperature']
            })

        if type == "MemoryUsage":
            for result in json_response['MemoryLoad']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Usage': result['Usage']
                })
            results.append({
                'Usage': json_response['AverageLoad']
            })

        if type == "GPSTemperature":
            for result in json_response['GpsTemperatureInfoModels']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Temperature': result['Temperature']
                })
            results.append({
                'Temperature': json_response['AverageTemperature']
            })

        # Not sure if values are correct
        if type == "DrivenDistance":
            results.append({
                'DrivenDistance': json_response['DistanceInMeters'],
            })
            results.append({
                'DrivenDistance': json_response['AverageDistanceInMeters']
            })

        # SPEEDTIMEINFO

        if type == "OperationTime":
            results.append({
                'OperationTime': json_response['OperationTimeInMinutes'],
            })
            results.append({
                'OperationTime': json_response['AverageOperationTimeInMinutes']
            })

    # Catch URLError exception
    except requests.exceptions.RequestException, e:
        print "Error querying API: ", e

    return results

################################################################################
###
###         get_processorusage(unitid), etc..
###
###         Prepares parsed data for frontend depending on data type.
###
################################################################################

def get_processorusage(unitid):
    usage = []
    datetime = []

    data = run_query("ProcessorUsage", unitid)
    average = data[ len(data) - 1 ]['Usage']

    for result in range(0, len(data) - 1):
        usage.append( data[result]['Usage'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    processorusage = {
        'Usage': usage,
        'DateTime': datetime,
        'Average': average,
    }

    return processorusage

def get_processortemperature(unitid):
    temperature = []
    datetime = []

    data = run_query("ProcessorTemperature", unitid)
    average = data[ len(data) - 1 ]['Temperature']

    for result in range(0, len(data) - 1):
        temperature.append( data[result]['Temperature'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    processortemperature = {
        'Temperature': temperature,
        'DateTime': datetime,
        'Average': average,
    }

    return processortemperature

def get_memoryusage(unitid):
    usage = []
    datetime = []

    data = run_query("MemoryUsage", unitid)
    average = data[ len(data) - 1 ]['Usage']

    for result in range(0, len(data) - 1):
        usage.append( data[result]['Usage'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    memoryusage = {
        'Usage': usage,
        'DateTime': datetime,
        'Average': average,
    }

    return memoryusage

def get_gpstemperature(unitid):
    temperature = []
    datetime = []

    data = run_query("GPSTemperature", unitid)
    average = data[ len(data) - 1 ]['Temperature']

    for result in range(0, len(data) - 1):
        temperature.append( data[result]['Temperature'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    gpstemperature = {
        'Temperature': temperature,
        'DateTime': datetime,
        'Average': average,
    }

    return gpstemperature

def get_drivendistance(unitid):
    distance = []

    data = run_query("DrivenDistance", unitid)
    raw_average = data[len(data) - 1]['DrivenDistance']
    average = float("{0:.2f}".format(raw_average / 1000))

    for result in range(0, len(data) - 1):
        raw_value = data[result]['DrivenDistance']
        rounded_value = float("{0:.2f}".format(raw_value / 1000))
        distance.append(rounded_value)

    drivendistance = {
        'Distance': distance,
        'Average': average,
    }

    return drivendistance

# speedtimeinfo

def get_operationtime(unitid):
    time = []

    data = run_query("OperationTime", unitid)
    raw_average = data[len(data) - 1]['OperationTime']
    average = float("{0:.2f}".format(raw_average))

    for result in range(0, len(data) - 1):
        raw_value = data[result]['OperationTime']
        rounded_value = float("{0:.2f}".format(raw_value))
        time.append(rounded_value)

    operationtime = {
        'Time': time,
        'Average': average,
    }

    return operationtime

def get_speedTimeObject(unitid):

        # Base URL
    root_url = "https://vms-hro.janhagenaar.eu/api/"
    data_type = "Position/GetSpeedTimeInfo"

    # Builds url
    request_url = root_url + data_type + "?unitId=" + str(unitid)

    result = object;

    try:
        # Connect
        response = requests.get(request_url)

        # Convert response to dictionary object
        result = response.json()


    # Catch URLError exception
    except requests.exceptions.RequestException, e:
        print "Error querying API: ", e

    return result
