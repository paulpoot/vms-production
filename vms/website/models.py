################################################################################
###
###         models.py
###         Last updated: 21/01/2016
###
###         Stores the various models for our database.
###
################################################################################
from django.db import models

################################################################################
###
###         Vehicle
###
###         Vehicle object. Gets created by populate_vehicles.py
###         Attributes:
###             unitid (UNIQUE): Vehicle ID
###             favourite: True if vehicle is marked as favourite
###             comments: Stores user's comments about the vehicle
###
################################################################################
class Vehicle(models.Model):
    unitid = models.BigIntegerField(unique=True)
    favourite = models.BooleanField(default=False)
    comments = models.TextField(blank=True, default='There are no comments about this vehicle yet.')

    def __str__(self):
        return str(self.unitid)
