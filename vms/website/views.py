################################################################################
###
###         views.py
###         Last updated: 21/01/2016
###
###         Handles the different views.
###
################################################################################
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from website.models import Vehicle
from website.api import get_processorusage, get_processortemperature, get_memoryusage, get_gpstemperature, get_drivendistance, get_operationtime, get_speedTimeObject
from website.populate_vehicles import populate
from website.forms import unitidForm
import requests
from io import BytesIO
from reportlab.pdfgen import canvas


################################################################################
###
###         COMMON PAGES
###
################################################################################
def index(request):
    return render(request, 'website/index.html')

def metadata(request):
    count = len(Vehicle.objects.all())
    vehicle_list = Vehicle.objects.order_by('unitid')

    context = {'count': count, 'vehicles': vehicle_list}

    return render(request, 'website/metadata.html', context)

################################################################################
###
###         LOGIN / LOGOUT
###
################################################################################

def user_login(request):

    alert_warning = '<div class="alert alert-warning" role="alert">'

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
                # We use request.POST.get('<variable>') as opposed to request.POST['<variable>'],
                # because the request.POST.get('<variable>') returns None, if the value does not exist,
                # while the request.POST['<variable>'] will raise key error exception
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                # An inactive account was used - no logging in!
                alert = alert_warning + "Your VMS account is disabled.</div>"
                return render(request, 'website/login.html', {'alert':alert})
        else:
            # Bad login details were provided. So we can't log the user in.
            alert = alert_warning + "Your login details couldn't be verified. Please check your input and try again.</div>"
            return render(request, 'website/login.html', {'alert':alert})

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request, 'website/login.html', {})

# Use @login_required to hide view when user is not logged in
@login_required
def user_logout(request):
    # We know user is logged in, so we can just log them out
    logout(request)

    return render(request, 'website/index.html')

################################################################################
###
###         VEHICLE OVERVIEW
###
################################################################################

@login_required
def vehicle_overview(request):
    # Query database for list of all vehicles sorted by unitid, ascending
    vehicle_list = Vehicle.objects.order_by('unitid')
    # Query database for list of all vehicles marked as favourites
    favourites = Vehicle.objects.filter(favourite=1)
    # Set list of vehicles as context, so it can be passed to the template

    context = {'vehicles': vehicle_list, 'favourites': favourites}

    return render(request, 'website/vehicle_overview.html', context)


@login_required
def search_vehicles(request):

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':

        vehicleid = request.POST.get('unitID')

    # Query the database for the unitid that the user has requested
    search_result = Vehicle.objects.filter(unitid__contains=vehicleid)

    context = {'results': search_result}

    return render(request, 'website/vehicle_overview.html', context)



################################################################################
###
###         VEHICLES
###
################################################################################

@login_required
def vehicle(request, vehicleid):
    # Context to pass to template
    context = {}

    # Try to find a vehicle with the passed unitid. If it exists, put it in the context.
    # If it doesn't, raise exception.
    try:
        vehicle = Vehicle.objects.get(unitid=vehicleid)
        favourite = vehicle.favourite

        processorusage = get_processorusage(vehicleid)
        processortemperature = get_processortemperature(vehicleid)
        memoryusage = get_processorusage(vehicleid)
        gpstemperature = get_gpstemperature(vehicleid)
        drivendistance = get_drivendistance(vehicleid)
        operationtime = get_operationtime(vehicleid)

        context = {
            'unitid': vehicle.unitid, 'comments': vehicle.comments, 'favourite': favourite,
            'processorusage_usage': processorusage['Usage'], 'processorusage_datetime': processorusage['DateTime'], 'processorusage_average': processorusage['Average'],
            'processortemperature_temperature': processortemperature['Temperature'], 'processortemperature_datetime': processortemperature['DateTime'], 'processortemperature_average': processortemperature['Average'],
            'memoryusage_usage': memoryusage['Usage'], 'memoryusage_datetime': memoryusage['DateTime'], 'memoryusage_average': memoryusage['Average'],
            'gpstemperature_temperature': gpstemperature['Temperature'], 'gpstemperature_datetime': gpstemperature['DateTime'], 'gpstemperature_average': gpstemperature['Average'],
            'drivendistance_distance': drivendistance['Distance'], 'drivendistance_average': drivendistance['Average'],
            'operationtime_time': operationtime['Time'], 'operationtime_average': operationtime['Average'],
        }
        return render(request, 'website/vehicle.html', context)
    except Vehicle.DoesNotExist:
        return redirect('website.views.vehicle_overview')

################################################################################
###
###         REPORTS
###
################################################################################

@login_required
def generate_reports(request, vehicleid):

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        results = request.POST

    graphs = []
    dd = False;
    ot = False;
    ct = False;
    cu = False;
    gt = False;
    ru = False;

    # Splitting the selected unitids from the selected graphs for the report.
    for result in results:
        if result != "csrfmiddlewaretoken":
                graphs.append(result)

    for graph in graphs:
    	if graph == "driven-distance":
    		dd = True
    	elif graph == "operation-time":
    		ot = True
    	elif graph == "cpu-temperature":
    		ct = True
    	elif graph == "cpu-usage":
    		cu = True
    	elif graph == "gps-temperature":
    		gt = True
    	elif graph == "ram-usage":
    		ru = True

        processorusage = get_processorusage(vehicleid)
        processortemperature = get_processortemperature(vehicleid)
        memoryusage = get_processorusage(vehicleid)
        gpstemperature = get_gpstemperature(vehicleid)
        drivendistance = get_drivendistance(vehicleid)
        operationtime = get_operationtime(vehicleid)

        context = {
            'unitid': vehicleid, 
            'processorusage_usage': processorusage['Usage'], 'processorusage_datetime': processorusage['DateTime'], 'processorusage_average': processorusage['Average'],
            'processortemperature_temperature': processortemperature['Temperature'], 'processortemperature_datetime': processortemperature['DateTime'], 'processortemperature_average': processortemperature['Average'],
            'memoryusage_usage': memoryusage['Usage'], 'memoryusage_datetime': memoryusage['DateTime'], 'memoryusage_average': memoryusage['Average'],
            'gpstemperature_temperature': gpstemperature['Temperature'], 'gpstemperature_datetime': gpstemperature['DateTime'], 'gpstemperature_average': gpstemperature['Average'],
            'drivendistance_distance': drivendistance['Distance'], 'drivendistance_average': drivendistance['Average'],
            'operationtime_time': operationtime['Time'], 'operationtime_average': operationtime['Average'],
            'drivendistance': dd,'operationtime': ot,'cputemperature': ct,'cpuusage': cu,'gpstemp': gt,'ramusage': ru,
        }

    return render(request, 'website/report.html', context)

@login_required
def get_metadata(request):

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':

        unit = request.POST.get('units')

	data = get_speedTimeObject(unit)

	response = HttpResponse(data, content_type='application/json')
	response['Content-Disposition'] = 'attachment; filename= metadata'
    
	return response


################################################################################
###
###         PYTHON SCRIPTS
###
################################################################################
@login_required
def populate_vehicles(request):
    populate()
    return redirect('website.views.vehicle_overview')

@login_required
def update_favourite(request, vehicleid, update):

    if update == "False":
        update = False

    v = Vehicle.objects.get_or_create(unitid=vehicleid)[0]
    v.favourite=update
    v.save()
    return redirect('website.views.vehicle', vehicleid=vehicleid)

@login_required
def update_comment(request, vehicleid):

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':

        comment = request.POST.get('comment')

    v = Vehicle.objects.get_or_create(unitid=vehicleid)[0]
    v.comments=comment
    v.save()

    return redirect('/vehicles/'+vehicleid+'/')
