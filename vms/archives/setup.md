# Setup
---
## Update your packages
First step is to update your packages:  
`sudo apt-get update`

## Setup your Virtual Environment
We use a Virtual Environment to keep our dependencies seperated from the rest of our system, so conflicts won't happen.  
`mkvirtualenv vms_django`  

You'll be working in the vms_django virtualenv after entering this command. When you're not in this virtualenv anymore (for example after a reboot) you will have to enter this to work on the vms_django environment:
`workon vms_django`

## Install packages
Now it's time to install the packages in the Virtual Environment. Make sure you're working in the right environment.
`sudo apt-get install django libpq-dev postgresql postgresql-contrib`
`pip3 install psycopg2`

## Setup Postgres database
It's time to create the Postgres database for Django. Enter the /vagrant/vms_django folder and execute:  
`sudo su postgres`  
`psql`  
`CREATE DATABASE djangodb;`  
`CREATE USER hhhhh WITH PASSWORD ‘root’;`
`ALTER ROLE hhhhh SET client_encoding TO 'utf8';`  
`ALTER ROLE hhhhh SET default_transaction_isolation TO 'read committed';`  
`ALTER ROLE hhhhh SET timezone TO 'UTC';`  
`GRANT ALL PRIVILEGES ON DATABASE djangodb TO hhhhh;"`  
`\q`  

## Configure Django
Now we have to execute a few commands so Django can use the database:  
`python3 manage.py makemigrations`  
`python3 manage.py migrate`
`python3 manage.py createsuperuser`  

Enter username `hhhhh` and password `root` when prompted.

You can now choose between restoring the database backup (/vagrant/vms_django/db_backup.sql) or building it from scratch. Follow the guide in postgres.md to learn how to restore the backup or rebuild the database from scratch by executing the populate_vehicles.py file.
