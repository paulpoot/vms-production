#!/bin/bash
source ~/.bashrcwi

sudo apt-get update

# Setup virtual environment for Django
mkvirtualenv vms_django
workon vms_django
pip3 install django

# Setup Postgres
sudo apt-get install libpq-dev postgresql postgresql-contrib
sudo -u postgres psql -c "CREATE DATABASE djangodb;
CREATE USER hhhhh WITH PASSWORD 'root';
ALTER ROLE hhhhh SET client_encoding TO 'utf8';
ALTER ROLE hhhhh SET default_transaction_isolation TO 'read committed';
ALTER ROLE hhhhh SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE djangodb TO hhhhh;"

# sudo su Postgres
# psql postgres -c "CREATE DATABASE djangodb;
# CREATE USER hhhhh WITH PASSWORD 'root';
# ALTER ROLE hhhhh SET client_encoding TO 'utf8';
# ALTER ROLE hhhhh SET default_transaction_isolation TO 'read committed';
# ALTER ROLE hhhhh SET timezone TO 'UTC';
# GRANT ALL PRIVILEGES ON DATABASE djangodb TO hhhhh;"
# exit

# Setup Postgres Django connection
pip3 install psycopg2

# Migrate DB
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py createsuperuser
