# Dockerfile for VMS

FROM python:2.7-onbuild

ENV DJANGO_CONFIGURATION Docker

CMD ["gunicorn", "-c", "gunicorn_conf.py", "--chdir", "vms", "vms.wsgi:application", "--reload"]
