# VMS Production Server
### Powered By Docker
***
Last updated: May 28th, 2016

Warning: the API that VMS connects to is probably down. Not all features of VMS will work.

## Requirements (Development Environment)
- Vagrant
- Virtualbox
- Git Bash (or similar)

This Readme is for Windows systems. If you're running a different OS, you might have to change some commands.

## How to use this

1. Open Git Bash and navigate to the project folder with the Vagrantfile.
2. Enter `vagrant up`.
3. When the box is done, enter `vagrant ssh` to login to the VM.
4. When you're in the VM, navigate to the vagrant folder with `cd /vagrant`
5. Now run the provision script. `sudo sh provision.sh`
6. Everything should be correctly installed now. Enter `sudo sh rebuild_docker.sh` to build and run the docker containers.
7. Enter `sudo docker-compose run django /bin/sh -c "cd vms; python manage.py migrate"` to prepare the database.
8. `sudo docker-compose run django /bin/sh -c "cd vms; python manage.py createsuperuser"` to create the superuser for the Django application.
9. Lastly, run `sudo docker-compose run django /bin/sh -c "cd vms; python manage.py collectstatic"` to collect the static files. Shouldn't be necessary when they haven't changed.
10. Open your browser on `localhost:8080`. You should be seeing the beautiful VMS website now :)

## Useful commands
- `sudo docker-compose run django bash` Enter bash shell of an existing, running Django container.
- `sudo docker rm -f $(sudo docker ps -a)` Force remove of all Docker containers.
- `sudo docker rmi -f $(sudo docker images)` Force remove of all Docker images.
